package id.ac.tazkia.landing.controller;

import id.ac.tazkia.landing.dao.FollowupDao;
import id.ac.tazkia.landing.dao.KabupatenKotaDao;
import id.ac.tazkia.landing.dao.SubscribeDao;
import id.ac.tazkia.landing.dao.TahunAjaranDao;
import id.ac.tazkia.landing.entity.*;
import org.flywaydb.core.internal.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.time.LocalDateTime;

@Controller
public class LandingController {

    private static final String DOMAIN_KARYAWAN = "kelaskaryawan.tazkia.ac.id";
    private static final String DOMAIN_REGULER = "beasiswa.tazkia.ac.id";
    private static final String DOMAIN_PASCA = "pascasarjana.tazkia.ac.id";
    private static final Logger logger = LoggerFactory.getLogger(LandingController.class);
    @Autowired
    private SubscribeDao subscribeDao;
    @Autowired private FollowupDao followupDao;
    @Autowired private TahunAjaranDao tahunAjaranDao;
    @Autowired private KabupatenKotaDao kabupatenKotaDao;
    @ModelAttribute("daftarKokab")
    public Iterable<KabupatenKota> daftarKokab(){
        return kabupatenKotaDao.findAll();
    }

    @GetMapping("/gagal")
    public void gagal(@RequestParam(value = "email", required = false) String email, Model model){
        if(email != null){
            model.addAttribute("email", email);
        }
    }
    @GetMapping("/selesai")
    public void selesai(@RequestParam(value = "email", required = false) String email, Model model){
        if(email != null){
            model.addAttribute("email", email);

        }
//
    }
    @GetMapping("/selesaiPameran")
    public void selesaiPameran(@RequestParam(value = "email", required = false) String email, Model model){
        if(email != null){
            model.addAttribute("email", email);

        }
//
    }
    @GetMapping("/selesaiKaryawan")
    public void selesaiKaryawan(@RequestParam(value = "email", required = false) String email, Model model) {
        if (email != null) {
            model.addAttribute("email", email);

        }
    }

    @GetMapping("/selesaiPasca")
    public void selesaiPasca(@RequestParam(value = "email", required = false) String email, Model model){
        if(email != null){
            model.addAttribute("email", email);

        }
    }

    @GetMapping("/sarjanaReguler")
    public String formIndex(@RequestParam(value = "utm_medium", required = false) String utm_medium, Model model){
        if (utm_medium != null) {
            model.addAttribute("utm_medium", utm_medium);
        }


        return "sarjanaReguler";
    }
//    @GetMapping("/")
//    public String form(@RequestParam(value = "utm_medium", required = false) String utm_medium, Model model){
//        if (utm_medium != null) {
//            model.addAttribute("utm_medium", utm_medium);
//        }
//
//
//        return "sarjanaReguler";
//    }

    @GetMapping("/")
    public String daftar(@RequestParam(value = "utm_medium", required = false) String utm_medium,
                         @RequestHeader(name="Host", required=false) final String host, Model model){
        if (utm_medium != null) {
            model.addAttribute("utm_medium", utm_medium);
        }

        logger.info("Hostname : {}", host);

        if(host.contains(DOMAIN_KARYAWAN)) {
            return "kelasKaryawan";
        } else if (host.contains(DOMAIN_REGULER)) {
            return "sarjanaReguler";
        } else if (host.contains(DOMAIN_PASCA)) {
            return "pascasarjana";
        }
        throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "domain tidak dikenali"
        );
    }

// Get Mapping Load Artikel Kelas reguler
    @GetMapping("/s1-reguler")
    public void s1reguler(){}

    @GetMapping("/s1-reguler-pendidikan")
    public void s1regulerpendidikan(){}


    @GetMapping("/s1-reguler-murah")
    public void s1regulermurah(){}

    @GetMapping("/s1-reguler-manajemen")
    public void s1regulermanajemen(){}

    @GetMapping("/s1-reguler-ilmu-komunikasi")
    public void s1regulerilmukomunikasi(){}


    @GetMapping("/s1-reguler-hukum")
    public void s1regulerhukum(){}

    @GetMapping("/s1-reguler-jakarta")
    public void s1regulerjakarta(){}

    @GetMapping("/s1-reguler-bandung")
    public void s1regulerbandung(){}

    @GetMapping("/s1-reguler-bekasi")
    public void s1regulerbekasi(){}

    @GetMapping("/s1-reguler-tangerang")
    public void s1regulertangerang(){}

    @GetMapping("/s1-reguler-kranggan")
    public void s1regulerkranggan(){}

    @GetMapping("/s1-reguler-ciawi")
    public void s1regulerciawi(){}

    @GetMapping("/s1-reguler-cianjur")
    public void s1regulercianjur(){}

    @GetMapping("/s1-reguler-cibubur")
    public void s1regulercibubur(){}


    @GetMapping("/s1-reguler-depok")
    public void s1regulerdepok(){}

    @GetMapping("/s1-reguler-bogor")
    public void s1regulerbogor(){}

    @GetMapping("/s1-reguler-akuntansi")
    public void s1regulerakuntansi(){}


    @PostMapping("/sarjanaReguler")
    public String  prosesSubscribe(@RequestParam(value = "utm_medium", required = false) String utm_medium,
                                  Subscribe subscribe, RedirectAttributes attributes)  {

        Subscribe cekEmail = subscribeDao.findByEmail(subscribe.getEmail());

        if (cekEmail == null) {
            subscribe.setStatus("None");
            subscribe.setTglInsert(LocalDateTime.now());

            Followup followup = followupDao.cariUserFuS1();
            Integer jumlah = Integer.valueOf(followup.getJumlah());
            Integer jmlh = jumlah + 1;
            followup.setJumlah(jmlh.toString());
            followupDao.save(followup);
            logger.info("follow up by {}", followup.getNama());

            if (subscribe.getKelasSma().equals("Sudah Lulus") || subscribe.getKelasSma().equals("12")) {
                TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
                subscribe.setTahunAjaran(tahunAjaran);
            } else {
                TahunAjaran tahunAjaran = tahunAjaranDao.findByKelas(subscribe.getKelasSma());
                subscribe.setTahunAjaran(tahunAjaran);
            }
            subscribe.setFollowup(followup);
            subscribe.setHubungi(0);
            subscribe.setSumber(utm_medium);
            subscribe.setKelas("Reguler");
            subscribe.setJenjang(Jenjang.S1);
            subscribeDao.save(subscribe);

            logger.info("Sukses : {} ", cekEmail);
            attributes.addFlashAttribute("selesai", "selesai");
            return "redirect:/selesai?email="+subscribe.getEmail();
        } else {
            logger.info("Gagal : {} ", cekEmail);
            return "redirect:/gagal?email="+subscribe.getEmail();
        }
    }

    @GetMapping("/pameran")
    public String formPemaran(@RequestParam(value = "utm_medium", required = false) String utm_medium, Model model){
        if (utm_medium != null) {
            model.addAttribute("utm_medium", utm_medium);
        }
        return "pameran";
    }

    @PostMapping("/pameran")
    public String  prosesPameran(@RequestParam(value = "utm_medium", required = false) String utm_medium,
                                   Subscribe subscribe, RedirectAttributes attributes)  {

        Subscribe cekEmail = subscribeDao.findByEmail(subscribe.getEmail());

        if (cekEmail == null) {
            subscribe.setStatus("None");
            subscribe.setTglInsert(LocalDateTime.now());

            Followup followup = followupDao.cariUserFuS1();
            Integer jumlah = Integer.valueOf(followup.getJumlah());
            Integer jmlh = jumlah + 1;
            followup.setJumlah(jmlh.toString());
            followupDao.save(followup);
            logger.info("follow up by {}", followup.getNama());

            if (subscribe.getKelasSma().equals("Sudah Lulus") || subscribe.getKelasSma().equals("12")) {
                TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);
                subscribe.setTahunAjaran(tahunAjaran);
            } else {
                TahunAjaran tahunAjaran = tahunAjaranDao.findByKelas(subscribe.getKelasSma());
                subscribe.setTahunAjaran(tahunAjaran);
            }
            subscribe.setFollowup(followup);
            subscribe.setHubungi(0);
            subscribe.setSumber(utm_medium);
            subscribe.setKelas("Reguler");
//            subscribe.setJenjang(Jenjang.S1);
            subscribeDao.save(subscribe);

            logger.info("Sukses : {} ", cekEmail);
            attributes.addFlashAttribute("selesai", "selesai");
            attributes.addFlashAttribute("utm_medium", utm_medium);
            return "redirect:/selesaiPameran?email="+subscribe.getEmail();
        } else {
            logger.info("Gagal : {} ", cekEmail);
            return "redirect:/gagal?email="+subscribe.getEmail();
        }
    }

    @GetMapping("/kelasKaryawan")
    public String formKelasKaryawan(@RequestParam(value = "utm_medium", required = false) String utm_medium, Model model){
        if (utm_medium != null) {
            model.addAttribute("utm_medium", utm_medium);
        }


        return "kelasKaryawan";
    }

// Get Mapping Load Artikel Kelas Karyawan
    @GetMapping("/s1-kelas-karyawan-murah-bogor")
    public void s1kkmurahbogor(){}

    @GetMapping("/s1-kelas-karyawan-manajemen-bisnis-murah")
    public void s1kkmanajemenbisnismurah(){}


    @GetMapping("/s1-kelas-karyawan-jakarta-manajemen-komunikasi")
    public void s1kkjakartamanajemenkomunikasi(){}

    @GetMapping("/s1-kelas-karyawan-jakarta-manajemen-bisnis-beasiswa")
    public void s1kkjakartamanajemenbisnisbeasiswa(){}

    @GetMapping("/s1-kelas-karyawan-jakarta-ilmu-komunikasi-beasiswa")
    public void s1kkjakartailmukomunikasibeasiswa(){}


    @GetMapping("/s1-kelas-karyawan-depok")
    public void s1kkdepok(){}

    @GetMapping("/s1-kelas-karyawan-tangerang")
    public void s1kktangerang(){}

    @GetMapping("/s1-kelas-karyawan-bekasi")
    public void s1kkbekasi(){}

    @GetMapping("/s1-kelas-karyawan-bandung")
    public void s1kkbandung(){}

    @GetMapping("/s1-kelas-karyawan-bogor")
    public void s1kkbogor(){}

    @GetMapping("/s1-kelas-karyawan-jakarta")
    public void s1kkjakarta(){}




    @GetMapping("/s1-kelas-karyawan-cepat-lulus-murah-depok")
    public void s1kkcepatlulusmurahdepok(){}

    @GetMapping("/s1-kelas-karyawan-bogor-komunikasi-murah")
    public void s1kkbogorkomunikasimurah(){}

    @GetMapping("/s1-kelas-karyawan-bogor-akuntansi-murah")
    public void s1kkbogorakuntansimurah(){}


    @GetMapping("/program-studi-kelas-karyawan")
    public void pskelaskaryawan(){}

    @GetMapping("/program-studi-kelas-karyawan-sore")
    public void pskelaskaryawansore(){}

    @GetMapping("/program-studi-kelas-karyawan-malam")
    public void pskelaskaryawanmalam(){}

    @GetMapping("/program-studi-kelas-karyawan-online")
    public void pskelaskaryawanonline(){}

    @GetMapping("/program-studi-kelas-karyawan-asal-sma")
    public void pskelaskaryawanasalsma(){}

    @GetMapping("/program-studi-kelas-karyawan-paket-c")
    public void pskelaskaryawanpaketc(){}



    @PostMapping("/kelasKaryawan")
    public String kelasKaryawan(@RequestParam(value = "utm_medium", required = false) String utm_medium,
                                Subscribe subscribe, RedirectAttributes attributes)  {
        Subscribe cekEmail = subscribeDao.findByEmail(subscribe.getEmail());

        if (cekEmail == null) {
            subscribe.setStatus("None");
            subscribe.setTglInsert(LocalDateTime.now());

            Followup followup = followupDao.cariUserFuKk();
            Integer jumlah = Integer.valueOf(followup.getJumlah());
            Integer jmlh = jumlah + 1;
            followup.setJumlah(jmlh.toString());
            followupDao.save(followup);
            logger.info("follow up by {}", followup.getNama());

            TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);

            subscribe.setTahunAjaran(tahunAjaran);
            subscribe.setFollowup(followup);
            subscribe.setHubungi(0);
            subscribe.setSumber(utm_medium);
            subscribe.setKelas("Karyawan");
            subscribe.setJenjang(Jenjang.S1);
            subscribeDao.save(subscribe);

            logger.info("Sukses : {} ", cekEmail);
            attributes.addFlashAttribute("selesai", "selesai");
            return "redirect:/selesaiKaryawan?email="+subscribe.getEmail();
        } else {
            logger.info("Gagal : {} ", cekEmail);
            return "redirect:/gagal?email="+subscribe.getEmail();
        }
    }

    @GetMapping("/pascasarjana")
    public String formPascasarjana(@RequestParam(value = "utm_medium", required = false) String utm_medium, Model model){
        if (utm_medium != null) {
            model.addAttribute("utm_medium", utm_medium);
        }

        return "pascasarjana";
    }

    // Get Mapping Load Artikel Pascasarjana
    @GetMapping("/s2-pascasarjana-murah-bogor")
    public void s2pascamurahbogor(){}

    @GetMapping("/s2-pascasarjana-magister-kuliah-online")
    public void s2pascasarjanamagisterkuliahonline(){}


    @GetMapping("/s2-kelas-karyawan-cepat-lulus-murah")
    public void s2karyawancepatlulusmurah(){}

    @GetMapping("/s2-kelas-karyawan-bogor")
    public void s2kelaskaryawanbogor(){}

    @GetMapping("/s2-magister-pascasarjana-gratis-uang-pangkal")
    public void s2gratisuangpangkal(){}


    @GetMapping("/Artikel/pascasarjana/s2-fleksibel")
    public void s2fleksibel(){}


    @GetMapping("/Artikel/pascasarjana/s2-ekonomi-gratis")
    public void s2ekonomigratis(){}

    @GetMapping("/s2-pascasarjana-magister-biaya-daftar-gratis")
    public void s2pascasarjanamagisterbiayagratis(){}

    @GetMapping("/s2-pascasarjana-magister-akuntansi-gratis")
    public void s2pascamagisterakuntansigratis(){}

    @GetMapping("/s2-pascasarjana-magister-ekonomi-gratis")
    public void s2pascamagisterekonomigratis(){}


    @GetMapping("/s2-magister-pascasarjana-kelas-lulus-1-5-tahun")
    public void s2magister1tahun(){}

    @PostMapping("/pascasarjana")
    public String pascasarjana(@RequestParam(value = "utm_medium", required = false) String utm_medium,
                                Subscribe subscribe, RedirectAttributes attributes)  {
        Subscribe cekEmail = subscribeDao.findByEmail(subscribe.getEmail());

        if (cekEmail == null) {
            subscribe.setStatus("None");
            subscribe.setTglInsert(LocalDateTime.now());

            Followup followup = followupDao.cariUserFuKk();
            Integer jumlah = Integer.valueOf(followup.getJumlah());
            Integer jmlh = jumlah + 1;
            followup.setJumlah(jmlh.toString());
            followupDao.save(followup);
            logger.info("follow up by {}", followup.getNama());

            TahunAjaran tahunAjaran = tahunAjaranDao.findByAktif(Status.AKTIF);

            subscribe.setTahunAjaran(tahunAjaran);
            subscribe.setFollowup(followup);
            subscribe.setHubungi(0);
            subscribe.setSumber(utm_medium);
            subscribe.setKelas("Reguler");
            subscribe.setJenjang(Jenjang.S2);
            subscribeDao.save(subscribe);

            logger.info("Sukses : {} ", cekEmail);
            attributes.addFlashAttribute("selesai", "selesai");
            return "redirect:/selesaiPasca?email="+subscribe.getEmail();
        } else {
            logger.info("Gagal : {} ", cekEmail);
            return "redirect:/gagal?email="+subscribe.getEmail();
        }
    }
}
