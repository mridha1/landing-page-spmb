package id.ac.tazkia.landing.dao;

import id.ac.tazkia.landing.entity.Followup;
import id.ac.tazkia.landing.entity.Subscribe;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SubscribeDao extends PagingAndSortingRepository<Subscribe,String> {
    Page<Subscribe> findByNamaContainingIgnoreCaseOrderByNamaDesc(String nama, Pageable page);

    Page<Subscribe> findByFollowup(Followup user, Pageable page);
    Subscribe findByEmail(String email);
}
