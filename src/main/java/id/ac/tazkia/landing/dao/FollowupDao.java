package id.ac.tazkia.landing.dao;

import id.ac.tazkia.landing.entity.Followup;
import id.ac.tazkia.landing.entity.Status;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface FollowupDao extends PagingAndSortingRepository<Followup,String> {

//    @Query(value = "select * from followup where status = 'AKTIF' order by jumlah ASC , id limit 1", nativeQuery = true)
//    Followup cariUserFu ();
    @Query(value = "select * from followup where status = 'AKTIF' and s1 = 'AKTIF' order by jumlah ASC , id limit 1", nativeQuery = true)
    Followup cariUserFuS1 ();

    @Query(value = "select * from followup where status = 'AKTIF' and s2 = 'AKTIF' order by jumlah ASC , id limit 1", nativeQuery = true)
    Followup cariUserFuS2 ();

    @Query(value = "select * from followup where status = 'AKTIF' and kk = 'AKTIF' order by jumlah ASC , id limit 1", nativeQuery = true)
    Followup cariUserFuKk ();
}
