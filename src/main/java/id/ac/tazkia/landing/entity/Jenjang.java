package id.ac.tazkia.landing.entity;

public enum Jenjang {
    S1,
    S2,
    D3
}
