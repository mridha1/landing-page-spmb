package id.ac.tazkia.landing.entity;

public enum Status {
    AKTIF,NONAKTIF,WAITING,APPROVED,REJECTED,
    FOLLOWUP, DONE, REPEAT
}
